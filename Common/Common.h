#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_COMMON_COMMON_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_COMMON_COMMON_H

#include "clang/AST/Stmt.h"

namespace clang {
namespace autosar {

/// If this is a group of SwitchCases that will fallthrough, get the last one.
///
///   ...
/// <SwitchCase>: // SC
/// <SwitchCase>:
/// <SwitchCase>:
/// <SwitchCase>: // returns this
/// <Stmt>
///   ...
///
const SwitchCase *getFallthroughCase(const SwitchCase *SC);

/// Is this Stmt a CXXThrowExpr or a BreakStmt
bool isBreakOrThrow(const Stmt *S);

/// Check if this is a break/throw or if it is a CompoundStmt check if it ends
/// in a break/throw.
bool isLastStmtTerminator(const Stmt *S);

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_COMMON_COMMON_H
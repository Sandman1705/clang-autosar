#include "Diags.h"

namespace clang {
namespace autosar {

unsigned getNewDiagID(DiagnosticsEngine &DE, const StringRef CheckName,
                      const StringRef Description,
                      DiagnosticIDs::Level Level /*= DiagnosticIDs::Warning*/) {

  return DE.getDiagnosticIDs()->getCustomDiagID(
      Level, (Description.str() + " [" + CheckName.str() + "]"));
}

} // namespace autosar
} // namespace clang

// RUN: clang-autosar --v-no-bool-condition %s -- 2>&1 | FileCheck %s
// RUN: clang-autosar --m-no-bool-condition %s -- 2>&1 | FileCheck %s

void test1(int x) {
  switch (x == 0) // CHECK: [[@LINE]]:11: warning: The condition of a switch statement shall not have bool type [M6-4-7 NoBoolCondition{{Visitor|Matcher}}]
  {
  case true:
    break;
  case false:
    break;
  default:
    break;
  }

  switch (x) {
  case 1:
    break;
  case 2:
    break;
  default:
    break;
  }
}

void test2(bool cond) {
  switch (cond) // CHECK: [[@LINE]]:11: warning: The condition of a switch statement shall not have bool type [M6-4-7 NoBoolCondition{{Visitor|Matcher}}]
  {
  case true:
    break;
  case false:
    break;
  default:
    break;
  }
}

// CHECK: 2 warnings generated
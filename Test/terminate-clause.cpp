// RUN: clang-autosar --v-terminate-clause %s -- 2>&1 | FileCheck %s
// RUN: clang-autosar --m-terminate-clause %s -- 2>&1 | FileCheck %s

void test(int x) {
  int n = 0;
  switch (x) {
  case 0:
    break;
  case 11:
  case 1:
    n = 1;
    ++n;
    break;
  case 22:
  case 2: {
    n = 2;
    break;
  }
  case 33:
  case 3: // CHECK: [[@LINE]]:3: warning: An unconditional throw or break statement shall terminate every non-empty switch-clause [M6-4-5 TerminateClause{{Visitor|Matcher}}]
    n = 3;
    ++n;
  case 44:
  case 4: { // CHECK: [[@LINE]]:3: warning: An unconditional throw or break statement shall terminate every non-empty switch-clause [M6-4-5 TerminateClause{{Visitor|Matcher}}]
    n = 4;
    ++n;
  }
  case 5: {
    n = 4;
    ++n;
  } break;
  case 6: { // CHECK: [[@LINE]]:3: warning: An unconditional throw or break statement shall terminate every non-empty switch-clause [M6-4-5 TerminateClause{{Visitor|Matcher}}]
    n = 4;
    ++n;
  } break;
    ++n;
  default: {
    throw;
  }
  }

  switch (x) {
    n++;
  }
}

void test2(int x) {
  int n = 0;
  switch (x) {
  case 0: { // CHECK: [[@LINE]]:3: warning: An unconditional throw or break statement shall terminate every non-empty switch-clause [M6-4-5 TerminateClause{{Visitor|Matcher}}]
    n = 1;
  case 1:
    break;
  }
  case 2: { // CHECK: [[@LINE]]:3: warning: An unconditional throw or break statement shall terminate every non-empty switch-clause [M6-4-5 TerminateClause{{Visitor|Matcher}}]
    n = 1;
    break;
  case 3:
    break;
  }
  // Code above is semantically equivalent to the following and has well
  // formated labels:
  // case 2: {
  //   n = 1;
  //   break;
  // }
  // case 3:
  //   break;
  default: {
    throw;
  }
  }
}

void test3(int x) {
  int n = 0;
  switch (x)
  {
  case 0: // CHECK: [[@LINE]]:3: warning: An unconditional throw or break statement shall terminate every non-empty switch-clause [M6-4-5 TerminateClause{{Visitor|Matcher}}]
    break;
  n = 0;
  default: // CHECK: [[@LINE]]:3: warning: An unconditional throw or break statement shall terminate every non-empty switch-clause [M6-4-5 TerminateClause{{Visitor|Matcher}}]
    break;
  ++n;
  }
}

// CHECK: 7 warnings generated
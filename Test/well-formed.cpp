// RUN: clang-autosar --v-well-formed %s -- 2>&1 | FileCheck %s
// RUN: clang-autosar --m-well-formed %s -- 2>&1 | FileCheck %s

void test(int x) {
  int n;
  switch (x) {
  int WarnSwitchStart; // CHECK-DAG: [[@LINE]]:3: warning: Switch statement should start with a switch case [M6-4-3 WellFormed{{Visitor|Matcher}}]
  case 0: {
    n = 0;
    break;
  }
  default: { // CHECK-DAG: [[@LINE]]:3: warning: Default case should be last [M6-4-3 WellFormed{{Visitor|Matcher}}]
    n = -1;
    break;
  }
  case 1: { // CHECK-DAG: [[@LINE]]:3: warning: Case clause should end with a break/throw statement or a compound statement that ends in break/throw. [M6-4-3 WellFormed{{Visitor|Matcher}}]
    n = 1;
  }
  case 2: {
    n = 2;
    break;
    case 3: // CHECK-DAG: [[@LINE]]:5: warning: There should be no case clause inside a case block [M6-4-3 WellFormed{{Visitor|Matcher}}]
      n = 3;
      break;
  }
  }

  switch (x) {
  default: { // CHECK-DAG: [[@LINE]]:3: warning: Default case should be last [M6-4-3 WellFormed{{Visitor|Matcher}}]
    n = -1;
    break;
  }
  case 1: {
    n = 1;
    break;
  }
  case 2: { // CHECK-DAG: [[@LINE]]:3: warning: Case clause should end with a break/throw statement or a compound statement that ends in break/throw. [M6-4-3 WellFormed{{Visitor|Matcher}}]
    n = 2;
    if (true)
      break;
  }
  case 3: {
    n = 3;
    throw 3;
  }
  case 4: // CHECK-DAG: [[@LINE]]:3: warning: Case clause should end with a break/throw statement or a compound statement that ends in break/throw. [M6-4-3 WellFormed{{Visitor|Matcher}}]
    return;
  case 5: {
    n = 5;
    case 6: // CHECK-DAG: [[@LINE]]:5: warning: There should be no case clause inside a case block [M6-4-3 WellFormed{{Visitor|Matcher}}]
      n = 6;
      break;
    break;
  }
  }

  switch (x) {
  case 1:
    n = 0;
    break;
  case 2:
    ++n;
    break;
  case 3: {
    --n;
  } break; // this is allowed
  default:
    throw -1;
  }
}

// CHECK: 8 warnings generated

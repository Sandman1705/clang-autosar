// RUN: clang-autosar --v-two-clause-minimum %s -- 2>&1 | FileCheck %s
// RUN: clang-autosar --m-two-clause-minimum %s -- 2>&1 | FileCheck %s

void test(int x) {
  switch (x) // CHECK: [[@LINE]]:3: warning: A switch statement shall have at least two case-clauses, distinct from the default label. [A6-4-1 TwoClauseMinimum{{Visitor|Matcher}}]
  {
  case 0:
    break;
  default:
    break;
  }

  switch (x)
  {
  case 0:
    break;
  case 1:
    break;
  default:
    break;
  }

  switch (x)
  {
  case 0:
  case 1:
    break;
  default:
    break;
  }

  switch (x) // CHECK: [[@LINE]]:3: warning: A switch statement shall have at least two case-clauses, distinct from the default label. [A6-4-1 TwoClauseMinimum{{Visitor|Matcher}}]
  {
  case 0:
    break;
  case 1:
  default:
    break;
  }

  int n;
  switch (x)
  {
  case 0:
    n = 0;
    break;
  case 1:
    n = 1;
  default:
    n = -1;
    break;
  }

  switch (x)
  {
  case 0:
  case 1:
    break;
  case 2:
  default:
    break;
  }

  switch (x) { // CHECK: [[@LINE]]:3: warning: A switch statement shall have at least two case-clauses, distinct from the default label. [A6-4-1 TwoClauseMinimum{{Visitor|Matcher}}]
  case 1:
  case 2:
  default:
  case 3:
  case 4:
    break;
  }

  switch (x) { // CHECK: [[@LINE]]:3: warning: A switch statement shall have at least two case-clauses, distinct from the default label. [A6-4-1 TwoClauseMinimum{{Visitor|Matcher}}]
  case 1:
  case 2:
  default:
  case 3:
  case 4:
    break;
  case 5:
    break;
  }

  switch (x) {
  case 1:
  case 2:
  default:
  case 3:
  case 4:
    break;
  case 5:
  case 6:
    break;
  }

  switch (x) { // CHECK: [[@LINE]]:3: warning: A switch statement shall have at least two case-clauses, distinct from the default label. [A6-4-1 TwoClauseMinimum{{Visitor|Matcher}}]
    case 0: {
      switch (x) {
      case 0:
        break;
      case 1:
        break;
      default:
        break;
      }
      break;
    }
    default:
      break;
  }
}

// CHECK: 5 warnings generated

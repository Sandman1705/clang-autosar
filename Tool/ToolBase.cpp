#include "ToolBase.h"
#include "../Visitors/StructuredLabel.h"
#include "../Visitors/TerminateClause.h"
#include "../Visitors/DefaultFinal.h"
#include "../Visitors/NoBoolCondition.h"
#include "../Visitors/TwoClauseMinimum.h"
#include "../Visitors/WellFormed.h"
#include "../Matchers/StructuredLabel.h"
#include "../Matchers/TerminateClause.h"
#include "../Matchers/DefaultFinal.h"
#include "../Matchers/NoBoolCondition.h"
#include "../Matchers/TwoClauseMinimum.h"
#include "../Matchers/WellFormed.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;

void AutosarConsumer::HandleTranslationUnit(ASTContext &AC) {
  Context.setSourceManager(&AC.getSourceManager());
  DiagnosticsEngine &DE = *Context.getDiagnosticsEngine();

  AC.setTraversalScope(TopLevelDecls);

  runVisitors(AC, DE);
  runMatchers(AC, DE);
}

void AutosarConsumer::runVisitors(ASTContext &AC, DiagnosticsEngine &DE) {
  if (Context.VStructuredLabel)
    StructuredLabelVisitor(AC, DE).TraverseAST(AC);
  if (Context.VTerminateClause)
    TerminateClauseVisitor(DE).TraverseAST(AC);
  if (Context.VDefaultFinal)
    DefaultFinalVisitor(DE).TraverseAST(AC);
  if (Context.VNoBoolCondition)
    NoBoolConditionVisitor(DE).TraverseAST(AC);
  if (Context.VTwoClauseMinimum)
    TwoClauseMinimumVisitor(DE).TraverseAST(AC);
  if (Context.VWellFormed)
    WellFormedVisitor(AC, DE).TraverseAST(AC);
}

void AutosarConsumer::runMatchers(ASTContext &AC, DiagnosticsEngine &DE) {

  llvm::SmallVector<MatchFinder::MatchCallback *, 6> UsedCallbacks;
  MatchFinder MF;

  auto setTraverse = [&](StatementMatcher matcher) {
    return Context.getSkipTemplated()
               ? traverse(clang::TK_IgnoreUnlessSpelledInSource, matcher)
               : matcher;
  };

  if (Context.MStructuredLabel) {
    StructuredLabelMatcher::Callback *Callback =
        new StructuredLabelMatcher::Callback(DE);
    MF.addMatcher(setTraverse(StructuredLabelMatcher::makeMatcher()), Callback);
    UsedCallbacks.push_back(Callback);
  }

  if (Context.MTerminateClause) {
    TerminateClauseMatcher::Callback *Callback =
        new TerminateClauseMatcher::Callback(DE);
    MF.addMatcher(setTraverse(TerminateClauseMatcher::makeMatcher()), Callback);
    UsedCallbacks.push_back(Callback);
  }

  if (Context.MDefaultFinal) {
    DefaultFinalMatcher::Callback *Callback =
        new DefaultFinalMatcher::Callback(DE);
    MF.addMatcher(setTraverse(DefaultFinalMatcher::makeMatcher()), Callback);
    UsedCallbacks.push_back(Callback);
  }

  if (Context.MNoBoolCondition) {
    NoBoolConditionMatcher::Callback *Callback =
        new NoBoolConditionMatcher::Callback(DE);
    MF.addMatcher(setTraverse(NoBoolConditionMatcher::makeMatcher()), Callback);
    UsedCallbacks.push_back(Callback);
  }

  if (Context.MTwoClauseMinimum) {
    TwoClauseMinimumMatcher::Callback *Callback =
        new TwoClauseMinimumMatcher::Callback(DE);
    MF.addMatcher(setTraverse(TwoClauseMinimumMatcher::makeMatcher()),
                  Callback);
    UsedCallbacks.push_back(Callback);
  }

  if (Context.MWellFormed) {
    WellFormedMatcher::Callback *Callback = new WellFormedMatcher::Callback(DE);
    MF.addMatcher(setTraverse(WellFormedMatcher::makeMatcher()), Callback);
    UsedCallbacks.push_back(Callback);
  }

  if (!UsedCallbacks.empty())
    MF.matchAST(AC);

  for (MatchFinder::MatchCallback *Callback : UsedCallbacks)
    delete Callback;
}

template <class T>
static bool isTemplateSpecializationKind(const NamedDecl *D,
                                  TemplateSpecializationKind Kind) {
  if (const auto *TD = dyn_cast<T>(D))
    return TD->getTemplateSpecializationKind() == Kind;
  return false;
}

static bool isTemplateSpecializationKind(const NamedDecl *D,
                                  TemplateSpecializationKind Kind) {
  return isTemplateSpecializationKind<FunctionDecl>(D, Kind) ||
         isTemplateSpecializationKind<CXXRecordDecl>(D, Kind) ||
         isTemplateSpecializationKind<VarDecl>(D, Kind);
}

static bool isImplicitTemplateInstantiation(const NamedDecl *D) {
  return isTemplateSpecializationKind(D, TSK_ImplicitInstantiation);
}

static bool isInsideMainFile(SourceLocation Loc, const SourceManager &SM) {
  if (!Loc.isValid())
    return false;
  FileID FID = SM.getFileID(SM.getExpansionLoc(Loc));
  return FID == SM.getMainFileID() || FID == SM.getPreambleFileID();
}

static bool isInSystemHeader(SourceLocation Loc, const SourceManager &SM) {
  if (!Loc.isValid())
    return false;
  return SM.isInSystemHeader(Loc);
}

bool AutosarConsumer::HandleTopLevelDecl(DeclGroupRef DG) {
  for (Decl *D : DG) {
    SourceManager &SM = D->getASTContext().getSourceManager();
    SourceLocation Loc = D->getLocation();

    if (Context.IgnoreIncludes && !isInsideMainFile(Loc, SM))
      continue;

    if (Context.IgnoreSystemIncludes && isInSystemHeader(Loc, SM))
      continue;

    if (Context.getSkipTemplated())
      if (const NamedDecl *ND = dyn_cast<NamedDecl>(D))
        if (isImplicitTemplateInstantiation(ND))
          continue;

    // ObjCMethodDecl are not actually top-level decls.
    if (isa<ObjCMethodDecl>(D))
      continue;

    TopLevelDecls.push_back(D);
  }

  return true;
}

} // namespace autosar
} // namespace clang

#include "Tool/ToolBase.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Core/Diagnostic.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "llvm/Support/Process.h"
#include <memory>

using namespace llvm;

// Apply a custom category to all command-line options so that they are the
// only ones displayed.
static cl::OptionCategory AutosarToolCategory("Autosar tool options");

// CommonOptionsParser declares HelpMessage with a description of the common
// command-line options related to the compilation database and input files.
// It's nice to have this help message in all tools.
static cl::extrahelp
    CommonHelp(clang::tooling::CommonOptionsParser::HelpMessage);

// A help message for this specific tool can be added afterwards.
static cl::extrahelp MoreHelp("\nMore help text...\n");

static cl::opt<bool> VWellFormed("v-well-formed", cl::desc(
R"(Run the visitor for Rule M6-4-3.
A switch statement shall be a well-formed switch statement.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> MWellFormed("m-well-formed", cl::desc(
R"(Run the matcher for Rule M6-4-3.
A switch statement shall be a well-formed switch statement.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> VStructuredLabel("v-structured-label", cl::desc(
R"(Run the visitor for Rule M6-4-4.
A switch-label shall only be used when the most closely-enclosing
compound statement is the body of a switch statement.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> MStructuredLabel("m-structured-label", cl::desc(
R"(Run the matcher for Rule M6-4-4.
A switch-label shall only be used when the most closely-enclosing
compound statement is the body of a switch statement.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> VTerminateClause("v-terminate-clause", cl::desc(
R"(Run the visitor for Rule M6-4-5.
An unconditional throw or break statement shall terminate every
non-empty switch-clause.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> MTerminateClause("m-terminate-clause", cl::desc(
R"(Run the matcher for Rule M6-4-5.
An unconditional throw or break statement shall terminate every
non-empty switch-clause.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> VDefaultFinal("v-default-final", cl::desc(
R"(Run the visitor for Rule M6-4-6.
The final clause of a switch statement shall be the default-clause.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> MDefaultFinal("m-default-final", cl::desc(
R"(Run the matcher for Rule M6-4-6.
The final clause of a switch statement shall be the default-clause.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> VNoBoolCondition("v-no-bool-condition", cl::desc(
R"(Run the visitor for Rule M6-4-7.
The condition of a switch statement shall not have bool type.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> MNoBoolCondition("m-no-bool-condition", cl::desc(
R"(Run the matcher for Rule M6-4-7.
The condition of a switch statement shall not have bool type.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> VTwoClauseMinimum("v-two-clause-minimum", cl::desc(
R"(Run the visitor for Rule A6-4-1.
A switch statement shall have at least two case-clauses,
distinct from the default label.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> MTwoClauseMinimum("m-two-clause-minimum", cl::desc(
R"(Run the matcher for Rule A6-4-1.
A switch statement shall have at least two case-clauses,
distinct from the default label.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> IgnoreIncludes("ignore-includes", cl::desc(
R"(Skip checking of code from #include directives.
By default all headers except system ones are checked.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> CheckSystemIncludes("check-system-includes", cl::desc(
R"(Enable checks for #include directives for system libraries.
Note that --ignore-includes option takes precedence.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> VAllChecks("v-all-checks", cl::desc(
R"(Run all available visitors.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

static cl::opt<bool> MAllChecks("m-all-checks", cl::desc(
R"(Run all available matches.
)"),
                                 cl::init(false), cl::cat(AutosarToolCategory));

int main(int argc, const char **argv) {
  auto ExpectedParser = clang::tooling::CommonOptionsParser::create(
      argc, argv, AutosarToolCategory);
  if (!ExpectedParser) {
    // Fail gracefully for unsupported options.
    llvm::errs() << ExpectedParser.takeError();
    return 1;
  }
  clang::tooling::CommonOptionsParser &OptionsParser = ExpectedParser.get();

  clang::autosar::AutosarContext Context;
  Context.IgnoreIncludes = IgnoreIncludes;
  Context.IgnoreSystemIncludes = !CheckSystemIncludes || IgnoreIncludes;

  Context.VWellFormed = VWellFormed || VAllChecks;
  Context.MWellFormed = MWellFormed || MAllChecks;
  Context.VStructuredLabel = VStructuredLabel || VAllChecks;
  Context.MStructuredLabel = MStructuredLabel || MAllChecks;
  Context.VTerminateClause = VTerminateClause || VAllChecks;
  Context.MTerminateClause = MTerminateClause || MAllChecks;
  Context.VDefaultFinal = VDefaultFinal || VAllChecks;
  Context.MDefaultFinal = MDefaultFinal || MAllChecks;
  Context.VNoBoolCondition = VNoBoolCondition || VAllChecks;
  Context.MNoBoolCondition = MNoBoolCondition || MAllChecks;
  Context.VTwoClauseMinimum = VTwoClauseMinimum || VAllChecks;
  Context.MTwoClauseMinimum = MTwoClauseMinimum || MAllChecks;

  clang::tooling::ClangTool Tool(OptionsParser.getCompilations(),
                                 OptionsParser.getSourcePathList());

  clang::autosar::AutosarFrontendActionFactory Factory(Context);

  clang::DiagnosticOptions *DiagOpts = new clang::DiagnosticOptions();
  DiagOpts->ShowColors = llvm::sys::Process::StandardOutHasColors();
  clang::DiagnosticConsumer *DiagConsumer =
      new clang::TextDiagnosticPrinter(llvm::errs(), DiagOpts);
  std::unique_ptr<clang::DiagnosticsEngine> DiagEngine =
      std::make_unique<clang::DiagnosticsEngine>(new clang::DiagnosticIDs,
                                                 DiagOpts, DiagConsumer);

  // TODO: Add option for suppressing diagnostics
  // Suppress autosar tool diagnostics
  // DiagEngine->setSuppressAllDiagnostics(true);

  Tool.setDiagnosticConsumer(DiagConsumer);
  Context.setDiagnosticsEngine(DiagEngine.get());

  return Tool.run(&Factory);
}
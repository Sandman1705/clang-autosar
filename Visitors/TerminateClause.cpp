#include "TerminateClause.h"
#include "../Common/Diags.h"
#include "../Common/Common.h"

namespace clang {
namespace autosar {

const StringRef TerminateClauseVisitor::CheckName =
    "M6-4-5 TerminateClauseVisitor";
const StringRef TerminateClauseVisitor::Description =
    "An unconditional throw or break statement shall terminate every non-empty "
    "switch-clause";

TerminateClauseVisitor::TerminateClauseVisitor(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

/// Starting from a given point in children of CompoundStmt get the last Stmt
/// before next SwitchCase (CaseStmt or DefaultStmt)
/// Returns current node if there isn't one.
///
///   ...
/// <SwitchCase>: // Curr
/// <Stmt>
/// <Stmt>
/// <Stmt>        // returns this
/// <SwitchCase>:
///   ...
///
static const Stmt *
getLastStmtBeforeNextCase(const CompoundStmt::const_body_iterator Curr,
                          const CompoundStmt::const_body_iterator End) {
  CompoundStmt::const_body_iterator LastStmt = Curr;
  CompoundStmt::const_body_iterator Next = std::next(LastStmt);
  while ((Next != End) && !isa<SwitchCase>(*Next)) {
    LastStmt = Next;
    Next = std::next(Next);
  }
  return *LastStmt;
}

bool TerminateClauseVisitor::VisitSwitchStmt(const SwitchStmt *SS) {
  const CompoundStmt *Body = dyn_cast_or_null<CompoundStmt>(SS->getBody());
  assert(Body && "SwitchStmt must have a CompoundStmt as body");

  for (CompoundStmt::const_body_iterator Curr = Body->body_begin(),
                                         End = Body->body_end();
       Curr != End; ++Curr) {

    const SwitchCase *SC = dyn_cast_or_null<SwitchCase>(*Curr);

    // Skip any Node that is not a SwitchCase
    if (!SC)
      continue;

    if (!isSwitchClauseTerminated(SC, Curr, End)) {
      // Get the last case of the fallthrough group.
      const SwitchCase *FC = getFallthroughCase(SC);

      DE.Report(FC->getBeginLoc(), DiagID);
    }
  }

  return true;
}

bool TerminateClauseVisitor::isSwitchClauseTerminated(
    const SwitchCase *SC, const CompoundStmt::const_body_iterator Curr,
    const CompoundStmt::const_body_iterator End) {
  // If there is more then one nested Stmt (be it break/throw or CompoundStmt)
  // they will be executed as part of current SwitchCase.
  // Get the last Stmt before next SwitchCase if it exists.
  const Stmt *LastStmt = getLastStmtBeforeNextCase(Curr, End);

  // If LastStmt exists...
  if (LastStmt != SC) {
    // ...but it is not a break or throw them we have a non-compliant case.
    if (!isBreakOrThrow(LastStmt)) {
      return false;
    }

  } else { // Explore the subtree of SwitchCase
    // Get the last case of the fallthrough group.
    const SwitchCase *FC = getFallthroughCase(SC);

    // Check if it is not terminated with break or throw.
    if (!isLastStmtTerminator(FC->getSubStmt())) {
      return false;
    }
  }
  return true;
}

} // namespace autosar
} // namespace clang

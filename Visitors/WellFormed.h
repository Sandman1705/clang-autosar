#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_WELL_FORMED_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_WELL_FORMED_H

#include "clang/AST/RecursiveASTVisitor.h"

namespace clang {
namespace autosar {

class WellFormedVisitor
    : public RecursiveASTVisitor<WellFormedVisitor> {
  ASTContext &AC;
  DiagnosticsEngine &DE;
  unsigned DiagIDSwitchStart;
  unsigned DiagIDDefaultFinal;
  unsigned DiagIDTerminateClause;
  unsigned DiagIDStructuredLabel;

  static const StringRef CheckName;
  static const StringRef Description;

  static const StringRef WarnSwitchStart;
  static const StringRef WarnDefaultFinal;
  static const StringRef WarnTerminateClause;
  static const StringRef WarnStructuredLabel;

  struct DefaultInfo {
    const DefaultStmt * Default = nullptr;
    bool HadCaseAfterDefault = false;
  };

  llvm::SmallVector<DefaultInfo, 4> SwitchStack;

public:
  WellFormedVisitor(ASTContext &AC, DiagnosticsEngine &DE);

  bool VisitSwitchCase(const SwitchCase * SC);
  bool VisitSwitchStmt(const SwitchStmt * SS);

  bool TraverseSwitchStmt(SwitchStmt *S);
  bool VisitCaseStmt(const CaseStmt *CS);
  bool VisitDefaultStmt(const DefaultStmt *DS);
};

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_WELL_FORMED_H
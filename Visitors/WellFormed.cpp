#include "WellFormed.h"
#include "../Common/Diags.h"
#include "../Common/Common.h"
#include "StructuredLabel.h"
#include "TerminateClause.h"
#include "clang/AST/ParentMapContext.h" // DynTypedNodeList

namespace clang {
namespace autosar {

const StringRef WellFormedVisitor::CheckName = "M6-4-3 WellFormedVisitor";
const StringRef WellFormedVisitor::Description =
    "A switch statement shall be a well-formed switch statement";

const StringRef WellFormedVisitor::WarnSwitchStart =
    "Switch statement should start with a switch case";
const StringRef WellFormedVisitor::WarnDefaultFinal =
    "Default case should be last";
const StringRef WellFormedVisitor::WarnTerminateClause =
    "Case clause should end with a break/throw statement or a compound "
    "statement that ends in break/throw.";
const StringRef WellFormedVisitor::WarnStructuredLabel =
    "There should be no case clause inside a case block";

WellFormedVisitor::WellFormedVisitor(ASTContext &AC, DiagnosticsEngine &DE)
    : AC(AC), DE(DE),
      DiagIDSwitchStart(getNewDiagID(DE, CheckName, WarnSwitchStart)),
      DiagIDDefaultFinal(getNewDiagID(DE, CheckName, WarnDefaultFinal)),
      DiagIDTerminateClause(getNewDiagID(DE, CheckName, WarnTerminateClause)),
      DiagIDStructuredLabel(getNewDiagID(DE, CheckName, WarnStructuredLabel)) {}

bool WellFormedVisitor::VisitSwitchCase(const SwitchCase *SC) {
  if (!StructuredLabelVisitor::isStructuredLabel(SC, AC))
    DE.Report(SC->getBeginLoc(), DiagIDStructuredLabel);

  return true;
}

bool WellFormedVisitor::VisitSwitchStmt(const SwitchStmt *SS) {

  const CompoundStmt *Body = dyn_cast_or_null<CompoundStmt>(SS->getBody());
  assert(Body && "SwitchStmt must have a CompoundStmt as body");

  if (!Body->body_empty() && !isa<SwitchCase>(Body->body_front()))
    DE.Report(Body->body_front()->getBeginLoc(), DiagIDSwitchStart);

  for (CompoundStmt::const_body_iterator Curr = Body->body_begin(),
                                         End = Body->body_end();
       Curr != End; ++Curr) {

    const SwitchCase *SC = dyn_cast_or_null<SwitchCase>(*Curr);

    // Skip any Node that is not a SwitchCase
    if (!SC)
      continue;

    if (!TerminateClauseVisitor::isSwitchClauseTerminated(SC, Curr, End)) {
      // Get the last case of the fallthrough group.
      const SwitchCase *FC = getFallthroughCase(SC);

      DE.Report(FC->getBeginLoc(), DiagIDTerminateClause);
    }
  }

  return true;
}

bool WellFormedVisitor::TraverseSwitchStmt(SwitchStmt *S) {
  SwitchStack.push_back({});

  // Visit subtree of SwitchStmt
  RecursiveASTVisitor<WellFormedVisitor>::TraverseSwitchStmt(S);

  // Check if this Switch did not have a default case
  if (!SwitchStack.back().Default)
    DE.Report(S->getBeginLoc(), DiagIDDefaultFinal);

  SwitchStack.pop_back();

  return true;
}

bool WellFormedVisitor::VisitCaseStmt(const CaseStmt *CS) {
  // If we are visiting a case after 'default' and haven't already reported it.
  if (SwitchStack.back().Default && !SwitchStack.back().HadCaseAfterDefault) {
    SwitchStack.back().HadCaseAfterDefault = true;
    DE.Report(SwitchStack.back().Default->getBeginLoc(), DiagIDDefaultFinal);
  }
  return true;
}

bool WellFormedVisitor::VisitDefaultStmt(const DefaultStmt *DS) {
  SwitchStack.back().Default = DS;
  return true;
}


} // namespace autosar
} // namespace clang

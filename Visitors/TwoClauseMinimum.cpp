#include "TwoClauseMinimum.h"
#include "../Common/Diags.h"

namespace clang {
namespace autosar {

const StringRef TwoClauseMinimumVisitor::CheckName =
    "A6-4-1 TwoClauseMinimumVisitor";
const StringRef TwoClauseMinimumVisitor::Description =
    "A switch statement shall have at least two case-clauses, distinct from "
    "the default label.";

TwoClauseMinimumVisitor::TwoClauseMinimumVisitor(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

/// Check if this SwitchCase will fallthrough to DefaultStmt.
///
/// <CaseStmt>    // SC
/// <CaseStmt>
/// <DefaultStmt> // return true
///   ...
///
/// <CaseStmt>  // SC
/// <CaseStmt>
/// <CaseStmt>
///   <Stmt>    // return false
///   ...
///
static bool hasFallthroughToDefault(const SwitchCase *SC) {
  while (const SwitchCase *Fallthrough =
             dyn_cast_or_null<SwitchCase>(SC->getSubStmt())) {
    if (isa<DefaultStmt>(Fallthrough))
      return true;
    SC = Fallthrough;
  }
  return false;
}

/// Get number of SwitchCase nodes starting after this case through which it
/// will fallthrough
///
/// <SwitchCase> // SC
/// <SwitchCase> // Count 1
/// <SwitchCase> // Count 2
///   <Stmt>     // return 2;
///
static unsigned countFallthroughGroup(const SwitchCase *SC) {
  unsigned Count = 0;
  while (const SwitchCase *Fallthrough =
             dyn_cast_or_null<SwitchCase>(SC->getSubStmt())) {
    SC = Fallthrough;
    ++Count;
  }
  return Count;
}

bool TwoClauseMinimumVisitor::VisitSwitchStmt(const SwitchStmt *SS) {
  int DistinctCases = 0;    // Number of cases distinct from default
  bool SeenDefault = false; // Has proceesed default from this SwitchStmt

  const SwitchCase *Curr = SS->getSwitchCaseList();
  while (Curr) {
    if (isa<DefaultStmt>(Curr)) {
      SeenDefault = true;
      // Any case to which this DefaultStmt can fallthrough will be incorrectly
      // counted as a case distinct from default due to order of the nodes.
      //
      // <DefaultStmt>
      // <CaseStmt>     // hasFallthroughToDefault gives false because of order
      // <CaseStmt>
      //   ...
      DistinctCases -= countFallthroughGroup(Curr);
    } else if (!hasFallthroughToDefault(Curr)) {
      ++DistinctCases;
    }

    // Only after we have seen both DefaultStmt and at least two cases distinct
    // from default can we claim that this SwitchStmt is compliant, since
    // DistinctCases count might decrease.
    if (SeenDefault && DistinctCases >= 2)
      return true;

    Curr = Curr->getNextSwitchCase();
  }

  if (DistinctCases < 2)
    DE.Report(SS->getBeginLoc(), DiagID);

  return true;
}

} // namespace autosar
} // namespace clang

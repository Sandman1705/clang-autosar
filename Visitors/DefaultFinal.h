#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_DEFAULT_FINAL_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_DEFAULT_FINAL_H

#include "clang/AST/RecursiveASTVisitor.h"

namespace clang {
namespace autosar {

class DefaultFinalVisitor
    : public RecursiveASTVisitor<DefaultFinalVisitor> {
  DiagnosticsEngine &DE;
  unsigned DiagID;

  static const StringRef CheckName;
  static const StringRef Description;

  struct DefaultInfo {
    const DefaultStmt * Default = nullptr;
    bool HadCaseAfterDefault = false;
  };

  llvm::SmallVector<DefaultInfo, 4> SwitchStack;

public:
  DefaultFinalVisitor(DiagnosticsEngine &DE);

  bool TraverseSwitchStmt(SwitchStmt *S);
  bool VisitCaseStmt(const CaseStmt *CS);
  bool VisitDefaultStmt(const DefaultStmt *DS);
};

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_DEFAULT_FINAL_H
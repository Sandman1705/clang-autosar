#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_NO_BOOL_CONDITION_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_NO_BOOL_CONDITION_H

#include "clang/AST/RecursiveASTVisitor.h"

namespace clang {
namespace autosar {

class NoBoolConditionVisitor
    : public RecursiveASTVisitor<NoBoolConditionVisitor> {
  DiagnosticsEngine &DE;
  unsigned DiagID;

  static const StringRef CheckName;
  static const StringRef Description;

public:
  NoBoolConditionVisitor(DiagnosticsEngine &DE);

  bool VisitSwitchStmt(const SwitchStmt *SS);
};

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_NO_BOOL_CONDITION_H
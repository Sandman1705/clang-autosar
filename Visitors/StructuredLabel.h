#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_STRUCTURED_LABEL_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_STRUCTURED_LABEL_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "llvm/ADT/SmallVector.h"

namespace clang {
namespace autosar {

class StructuredLabelVisitor
    : public RecursiveASTVisitor<StructuredLabelVisitor> {
  ASTContext &AC;
  DiagnosticsEngine &DE;
  unsigned DiagID;

  static const StringRef CheckName;
  static const StringRef Description;

public:
  StructuredLabelVisitor(ASTContext &AC, DiagnosticsEngine &DE);

  bool VisitSwitchCase(const SwitchCase *SC);

  static bool isStructuredLabel(const SwitchCase *SC, ASTContext &AC);
};

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_VISITORS_STRUCTURED_LABEL_H
#include "DefaultFinal.h"
#include "../Common/Diags.h"

namespace clang {
namespace autosar {

const StringRef DefaultFinalVisitor::CheckName = "M6-4-6 DefaultFinalVisitor";
const StringRef DefaultFinalVisitor::Description =
    "The final clause of a switch statement shall be the default-clause";

DefaultFinalVisitor::DefaultFinalVisitor(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

bool DefaultFinalVisitor::TraverseSwitchStmt(SwitchStmt *S) {
  SwitchStack.push_back({});

  // Visit subtree of SwitchStmt
  RecursiveASTVisitor<DefaultFinalVisitor>::TraverseSwitchStmt(S);

  // Check if this Switch did not have a default case
  if (!SwitchStack.back().Default)
    DE.Report(S->getBeginLoc(), DiagID);

  SwitchStack.pop_back();

  return true;
}

bool DefaultFinalVisitor::VisitCaseStmt(const CaseStmt *CS) {
  // If we are visiting a case after 'default' and haven't already reported it.
  if (SwitchStack.back().Default && !SwitchStack.back().HadCaseAfterDefault) {
    SwitchStack.back().HadCaseAfterDefault = true;
    DE.Report(SwitchStack.back().Default->getBeginLoc(), DiagID);
  }
  return true;
}

bool DefaultFinalVisitor::VisitDefaultStmt(const DefaultStmt *DS) {
  SwitchStack.back().Default = DS;
  return true;
}

} // namespace autosar
} // namespace clang

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_MATCHERS_WELL_FORMED_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_MATCHERS_WELL_FORMED_H

#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "llvm/ADT/SmallVector.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;

namespace WellFormedMatcher {

class Callback : public MatchFinder::MatchCallback {
  DiagnosticsEngine &DE;
  unsigned DiagIDSwitchStart;
  unsigned DiagIDDefaultFinal;
  unsigned DiagIDTerminateClause;
  unsigned DiagIDStructuredLabel;

  static const StringRef CheckName;
  static const StringRef Description;

  static const StringRef WarnSwitchStart;
  static const StringRef WarnDefaultFinal;
  static const StringRef WarnTerminateClause;
  static const StringRef WarnStructuredLabel;

public:
  Callback(DiagnosticsEngine &DE);
  virtual void run(const MatchFinder::MatchResult &Result) override;
};

StatementMatcher makeMatcher();

}

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_MATCHERS_WELL_FORMED_H
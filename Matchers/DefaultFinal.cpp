#include "DefaultFinal.h"
#include "../Common/Diags.h"
#include "HelperMatchers.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;
using namespace clang::ast_matchers::internal;

const StringRef DefaultFinalMatcher::Callback::CheckName =
    "M6-4-6 DefaultFinalMatcher";
const StringRef DefaultFinalMatcher::Callback::Description =
    "The final clause of a switch statement shall be the default-clause";

DefaultFinalMatcher::Callback::Callback(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

void DefaultFinalMatcher::Callback::run(
    const MatchFinder::MatchResult &Result) {
  const BoundNodes &BN = Result.Nodes;
  // If default is not the last SwitchCase then report a warning pointing to
  // that DefaultStmt.
  if (const DefaultStmt *DS = BN.getNodeAs<DefaultStmt>("default"))
    DE.Report(DS->getBeginLoc(), DiagID);
  // If there is no DefaultStmt then report a warning pointing to the
  // SwitchStmt.
  else if (const SwitchStmt *SS = BN.getNodeAs<SwitchStmt>("switch"))
    DE.Report(SS->getBeginLoc(), DiagID);
}

StatementMatcher DefaultFinalMatcher::makeMatcher() {
  // Bind enclosing SwitchStmt.
  auto matchParentSwitch = hasAncestor(switchStmt().bind("pswitch"));

  // Check if the enclosing SwitchStmt is the same as the SwitchStmt bound with
  // "switch". Needed when there are nested SwitchStmts.
  auto isInSameSwitch = allOf(
      matchParentSwitch, hasAncestor(switchStmt(equalsBoundNode("switch"),
                                                equalsBoundNode("pswitch"))));

  // Bind the DefaultStmt in the same SwitchStmt.
  auto matchDefault =
      hasDescendant(defaultStmt(isInSameSwitch).bind("default"));

  // Match a CaseStmt after the "default" node in the same SwitchStmt.
  auto caseAfterDefault =
      hasDescendant(caseStmt(afterBoundNode("default"), isInSameSwitch));

  // Match if there is a CaseStmt after the DefaultStmt in same SwitchStmt.
  auto defaultNotLast = allOf(matchDefault, caseAfterDefault);

  // Match if there is not DefaultStmt in the SwitchStmt
  auto noDefault = unless(hasDescendant(defaultStmt(isInSameSwitch)));

  // Match every switchStmt that either:
  // - Has no default case (noDefault).
  // - Has a CaseStmt after the default one (defaultNotLast).
  return switchStmt(stmt().bind("switch"), anyOf(noDefault, defaultNotLast));
}

} // namespace autosar
} // namespace clang

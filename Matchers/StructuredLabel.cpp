#include "StructuredLabel.h"
#include "../Common/Diags.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;

const StringRef StructuredLabelMatcher::Callback::CheckName =
    "M6-4-4 StructuredLabelMatcher";
const StringRef StructuredLabelMatcher::Callback::Description =
    "A switch-label shall only be used when the most closely-enclosing "
    "compound statement is the body of a switch statement";

StructuredLabelMatcher::Callback::Callback(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

void StructuredLabelMatcher::Callback::run(
    const MatchFinder::MatchResult &Result) {
  const BoundNodes &BN = Result.Nodes;
  if (const SwitchCase *SC = BN.getNodeAs<SwitchCase>("unstructured"))
    DE.Report(SC->getBeginLoc(), DiagID);
}

StatementMatcher StructuredLabelMatcher::makeMatcher() {
  return switchCase(unless(hasParent(stmt(anyOf(
                        switchCase(), compoundStmt(hasParent(switchStmt())))))))
      .bind("unstructured");
}

} // namespace autosar
} // namespace clang

#include "TwoClauseMinimum.h"
#include "../Common/Diags.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;
using namespace clang::ast_matchers::internal;

const StringRef TwoClauseMinimumMatcher::Callback::CheckName =
    "A6-4-1 TwoClauseMinimumMatcher";
const StringRef TwoClauseMinimumMatcher::Callback::Description =
    "A switch statement shall have at least two case-clauses, distinct from "
    "the default label.";

// Check if there is a DefaultStmt above this SwitchCase in the same fallthrough
// group.
AST_MATCHER(SwitchCase, hasDefaultAbove) {
  const SwitchCase *SC = &Node;
  while (SC) {
    const auto &Parents = Finder->getASTContext().getParents(*SC);
    if (Parents.size() < 1)
      return false;
    if (Parents[0].get<DefaultStmt>())
      return true;
    SC = Parents[0].get<SwitchCase>();
  }
  return false;
}

// Check if there is a DefaultStmt bellow this SwitchCase in the same
// fallthrough group.
AST_MATCHER(SwitchCase, hasDefaultBelow) {
  const SwitchCase *SC = &Node;
  while (SC) {
    if (isa<DefaultStmt>(SC->getSubStmt()))
      return true;
    SC = dyn_cast_or_null<SwitchCase>(SC->getSubStmt());
  }
  return false;
}

TwoClauseMinimumMatcher::Callback::Callback(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

void TwoClauseMinimumMatcher::Callback::run(
    const MatchFinder::MatchResult &Result) {
  const BoundNodes &BN = Result.Nodes;

  if (const SwitchStmt *SS = BN.getNodeAs<SwitchStmt>("switch"))
    DE.Report(SS->getBeginLoc(), DiagID);
}

StatementMatcher TwoClauseMinimumMatcher::makeMatcher() {
  // Bind enclosing SwitchStmt.
  auto matchParentSwitch = hasAncestor(switchStmt().bind("pswitch"));

  // Check if the enclosing SwitchStmt is the same as the SwitchStmt bound with
  // "switch". Needed when there are nested SwitchStmts.
  auto isInSameSwitch = allOf(
      matchParentSwitch, hasAncestor(switchStmt(equalsBoundNode("switch"),
                                                equalsBoundNode("pswitch"))));

  // Check if there is a DefaultStmt in the fallthorugh group containing this
  // SwitchCase. Such a SwitchCase will be distinct from default.
  auto noDefaultInFallthroughGroup =
      unless(anyOf(hasDefaultAbove(), hasDefaultBelow()));

  // Match two descendant nodes that are CaseStmts:
  // - They must be a SwitchCase that is part of the original SwitchStmt and not
  // from a nested one (isInSameSwitch).
  // - They must not contain a DefaultStmt in their fallthrough group
  // (noDefaultInFallthroughGroup).
  // - Check that second matched node is different from first.
  auto twoCasesDistinctFromDefault = allOf(
      hasDescendant(
          caseStmt(isInSameSwitch, noDefaultInFallthroughGroup).bind("case1")),
      hasDescendant(caseStmt(unless(equalsBoundNode("case1")), isInSameSwitch,
                             noDefaultInFallthroughGroup)));

  // Match every SwitchStmt that does not have two CaseStmts distinct from
  // default.
  return switchStmt(stmt().bind("switch"), unless(twoCasesDistinctFromDefault));
}

} // namespace autosar
} // namespace clang

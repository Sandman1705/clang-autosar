#include "TerminateClause.h"
#include "../Common/Common.h"
#include "../Common/Diags.h"
#include "HelperMatchers.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchersInternal.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;
using namespace clang::ast_matchers::internal;

const StringRef TerminateClauseMatcher::Callback::CheckName =
    "M6-4-5 TerminateClauseMatcher";
const StringRef TerminateClauseMatcher::Callback::Description =
    "An unconditional throw or break statement shall terminate every non-empty "
    "switch-clause";

/// Match node immediately before this in the enclosing Stmt.
AST_MATCHER_P(Stmt, hasPreviousSibiling, Matcher<Stmt>,
              InnerMatcher) {
  const auto &Parents = Finder->getASTContext().getParents(Node);
  const Stmt *Previous = nullptr;

  if (const Stmt *Parent = Parents[0].get<Stmt>()) {
    for (const Stmt *Child : Parent->children()) {
      if (Child == &Node)
        return (Previous != nullptr &&
                InnerMatcher.matches(*Previous, Finder, Builder));
      Previous = Child;
    }
  }
  return (Previous != nullptr &&
          InnerMatcher.matches(*Previous, Finder, Builder));
}

/// Check if this Stmt is terminating according to Rule M6-4-5
AST_MATCHER(Stmt, terminatingStmt) {
  return isLastStmtTerminator(&Node);
}

/// Match last Stmt from this code block (CompoundStmt)
AST_MATCHER_P(CompoundStmt, hasLastStmt, Matcher<Stmt>,
              InnerMatcher) {
  return Node.body_back() &&
         InnerMatcher.matches(*Node.body_back(), Finder, Builder);
}

/// If this is a group of SwitchCases that will fallthrough, match the last one.
AST_MATCHER_P(SwitchCase, getFallthroughCase,
              Matcher<SwitchCase>, InnerMatcher) {
  const SwitchCase *SC = getFallthroughCase(&Node);
  return InnerMatcher.matches(*SC, Finder, Builder);
}

/// Match subStmt of this SwitchCase.
AST_MATCHER_P(SwitchCase, hasSubStmt, Matcher<Stmt>,
              InnerMatcher) {
  return InnerMatcher.matches(*Node.getSubStmt(), Finder, Builder);
}

TerminateClauseMatcher::Callback::Callback(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

void TerminateClauseMatcher::Callback::run(
    const MatchFinder::MatchResult &Result) {
  const BoundNodes &BN = Result.Nodes;
  // If the last Stmt is terminating this SwitchCase is compliant.
  if (BN.getNodeAs<Stmt>("terminating"))
    return;

  // Even if SwitchCase's SubStmt is terminating it's compliant only if there is
  // not a Stmt after it and before next SwitchCase.
  if (BN.getNodeAs<Stmt>("terminatingSub") &&
      BN.getNodeAs<SwitchCase>("laststmt"))
    return;

  // Every other case is not compliant
  if (const SwitchCase *SC = BN.getNodeAs<SwitchCase>("case"))
    DE.Report(SC->getBeginLoc(), DiagID);
}

StatementMatcher TerminateClauseMatcher::makeMatcher() {
  // Check if this is a nested SwitchCase.
  auto isNestedCase = hasParent(switchCase());

  // Get Stmt immediatly before this one in the same enclosing CompoundStmt.
  // If there isn't one the bind against starting SwitchCase.
  // Optionally bind as terminating if it ends with break/throw.
  auto bindPreviousSibiling = hasPreviousSibiling(
      stmt(optionally(stmt(terminatingStmt()).bind("terminating")))
          .bind("laststmt"));

  // Match first SwitchCase after current "case".
  // Also bind Stmt immediatly before this
  auto nextCase =
      switchCase(afterBoundNode("case"), bindPreviousSibiling);

  // Call matcher for binding last Stmt before next SwitchCase in the same
  // enclosing CompoundStmt (SwitchStmt).
  auto lastStmtbeforeNextCase = hasParent(compoundStmt(has(nextCase)));

  // Optionally bind last Stmt in this SwitchCase's substatement if it is
  // terminating.
  auto lastSubStmt = getFallthroughCase(switchCase(hasSubStmt(
      stmt(optionally(stmt(terminatingStmt()).bind("terminatingSub"))))));

  // Last SwitchCase will fail to match on nextCase so we handle this case
  // seperatly. This will match last SwitchCase in a SwitchStmt.
  auto lastCase =
      hasParent(compoundStmt(hasParent(switchStmt()),
                             unless(has(switchCase(afterBoundNode("case"))))));

  // Last stmt of the last SwitchCase is also last Stmt in SwitchStmt.
  // Optionally bind that stmt if it is terminating.
  auto lastSwitchStmt = hasParent(compoundStmt(
      hasLastStmt(stmt(optionally(stmt(terminatingStmt()).bind("terminating")))
                      .bind("laststmt"))));

  // Bind SwitchCase that is last in the fallthrough group.
  auto lastCaseInGroup = allOf(getFallthroughCase(switchCase().bind("case")),
                               unless(isNestedCase));

  // Match SwitchCase per fallthrough group ("case") alongside with:
  // - Binding for Last Stmt in same enclosing CompoundStmt before next
  //   SwitchCase ("laststmt")
  // - Optional binding for "laststmt" as "terminating" if it ends with
  //   break/throw.
  // - Optional binding for last Stmt in SwitchCase's subStmt() as "terminating"
  //   if it ends with break/throw.
  return switchCase(lastCaseInGroup,
                    anyOf(allOf(lastStmtbeforeNextCase, lastSubStmt),
                          allOf(lastCase, lastSwitchStmt, lastSubStmt)));
}

} // namespace autosar
} // namespace clang

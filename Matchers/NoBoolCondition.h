#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_MATCHERS_NO_BOOL_CONDITION_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_MATCHERS_NO_BOOL_CONDITION_H

#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "llvm/ADT/SmallVector.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;

namespace NoBoolConditionMatcher {

class Callback : public MatchFinder::MatchCallback {
  DiagnosticsEngine &DE;
  const unsigned DiagID;

  static const StringRef CheckName;
  static const StringRef Description;

public:
  Callback(DiagnosticsEngine &DE);
  virtual void run(const MatchFinder::MatchResult &Result) override;
};

StatementMatcher makeMatcher();

}

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_AUTOSAR_MATCHERS_NO_BOOL_CONDITION_H
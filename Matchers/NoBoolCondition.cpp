#include "NoBoolCondition.h"
#include "../Common/Diags.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;
using namespace clang::ast_matchers::internal;

const StringRef NoBoolConditionMatcher::Callback::CheckName =
    "M6-4-7 NoBoolConditionMatcher";
const StringRef NoBoolConditionMatcher::Callback::Description =
    "The condition of a switch statement shall not have bool type";

NoBoolConditionMatcher::Callback::Callback(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

void NoBoolConditionMatcher::Callback::run(
    const MatchFinder::MatchResult &Result) {
  const BoundNodes &BN = Result.Nodes;

  if (const Expr *E = BN.getNodeAs<Expr>("condition"))
    DE.Report(E->getBeginLoc(), DiagID) << E->getSourceRange();
}

StatementMatcher NoBoolConditionMatcher::makeMatcher() {
  return switchStmt(hasCondition(
      expr(ignoringImpCasts(hasType(booleanType()))).bind("condition")));
}

} // namespace autosar
} // namespace clang

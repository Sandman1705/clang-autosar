#include "HelperMatchers.h"

namespace clang {
namespace autosar {

SourceLocation AfterBoundNodePredicate::getBeginLoc(
    const clang::DynTypedNode &Node) {
  SourceLocation Loc;
  if (const Decl *D = Node.get<Decl>())
    Loc = D->getBeginLoc();
  else if (const Stmt *S = Node.get<Stmt>())
    Loc = S->getBeginLoc();
  return Loc;
}

bool AfterBoundNodePredicate::operator()(const BoundNodesMap &Nodes) const {
  const SourceLocation S1 = getBeginLoc(Nodes.getNode(ID));
  const SourceLocation S2 = getBeginLoc(Node);

  if (S1.isInvalid() || S2.isInvalid())
    return false;

  return !SM.isBeforeInTranslationUnit(S1, S2);
}

} // namespace autosar
} // namespace clang

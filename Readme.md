# Clang-Autosar

Automated checker of Autosar coding guidelines built with LLVM, Clang and LibTooling.

## Building the project

Requires LLVM from: https://github.com/llvm/llvm-project

LLVM Build instructions: https://llvm.org/docs/GettingStarted.html

  * Latest tested LLVM revision: e7796c9673c19dbbdc9a3911692d04f9981272b5

  * Latest tested stable release: release/15.x

Can be built either as a clang-tool or a tool in clang-tool-extra

### Full build instructions including LLVM from scratch:

1. Checkout LLVM and clang-autosar project:

  * ``git clone https://github.com/llvm/llvm-project.git``

    * Checkout to a stable branch if you wish:

    * ``git checkout release/15.x``

  * ``cd llvm-project/clang-tools-extra``

  * ``git clone https://gitlab.com/Sandman1705/clang-autosar.git``

2. Configure and build LLVM/Clang with our new tool:

  * Add our new project in CMakeLists for clang-tools-extra.
    Make sure that the name of directory cloned by command above matches the name of subdirectory you are adding to CMakeLists.txt

    * ``cd llvm-project/clang-tools-extra`` if you are not positioned there already

    * ``echo "add_subdirectory(clang-autosar)" >> CMakeLists.txt``

  * Build LLVM as you regulary would with clang-tools-extra.
    For example:

    * ``cd ../`` Return to llvm-project root directory

    * ``mkdir build-release``

    * ``cd build-release``

    * ``cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra" -DLLVM_TARGETS_TO_BUILD=X86 -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_EXPORT_COMPILE_COMMANDS=On ../llvm``

      * Make sure to use ``-DCMAKE_EXPORT_COMPILE_COMMANDS=On`` if you wish to run tool over source files of LLVM itself.

      * ``ln -s $PWD/compile_commands.json ../`` (if you have not changed your positioning) or ``ln -s $PWD/compile_commands.json path/to/llvm/source/``

        * This will link our LLVM source with apporpriate ``compile_commands.json``

  * Alternatively you could add this tool in clang/tools instead and update clang/tools/CMakeLists.txt. That way you can also remove clang-tools-extra from ``-DLLVM_ENABLE_PROJECTS``.

  * Regardless of whether you added this as a clang-tool or clang-tools-extra you can build it with ninja (or make depening on what you chose):

  * Build either everything with ``ninja`` of just what you need with options below:

    * ``ninja clang-autosar``

    * Run the tests with:

    * ``ninja check-clang-autosar``

      * This will probably build various clang unit tests. If you wish to avoid that you can run tests directly with llvm-lit tool used for testing.

      * You will probably need to build FileCheck tool first which is used by llvm-lit.

        * ``ninja FileCheck``

      * ``build-release/bin/llvm-lit clang-tools-extra/clang-autosar/Test/ -sv`` From llvm-project root directory

    * Final step for ``compile_commands.json`` to work is to build table-generated .cpp files. Since there is no specific command for that, the safest way is to build entire project, or at least libraries/subprojects you plan to run the tool on.

      * ``ninja clang`` or just ``ninja``

### Tool use:

1. Simple use case:

  * ``./clang-autosar [options] test.cpp --``

    * ``--`` can be added to skip loading a compilation database

  * Use ``./clang-autosar --help`` to see the list of available options

2. Run tool on source files of LLVM:

  * Make sure ``compile_commands.json`` is properly generated and linked to source tree. Also make sure necessary tablegen'd c++ files are present (or just check if entire project is built)

  * ``./clang-autosar --v-all-checks .../path/to/llvm/clang/lib/Sema/SemaExpr.cpp``
